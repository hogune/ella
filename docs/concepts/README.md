---
title: Concepts
---

# Concepts

These pages provide in-depth backgound on key aspects of *ella* that are important for the end-user, but that are not immediately visible in the user interface. 

Further specification for the technically minded can be found in the [Technical documentation](/technical/).

### Contents

- [Workflows](/concepts/workflows.md): Differences between ANALYSES and VARIANTS workflows.
- [Filtering](/concepts/filtering.md): Explanation of the different available filters in *ella*, and how to configure.
- [ACMG rules engine](/concepts/acmg-rule-engine.md): Explanation of how the rules engine for generating suggested ACMG criteria and classifications works.
- [Gene panels](/concepts/gene-panels.md): Explanation of how genes are selected and configured for *ella*.