---
sidebar: false
---

<div style="text-align: center;padding-top: 50px;padding-bottom: 50px">
	<img width="250px;" src="./logo_blue.svg">
	<div style="font-size: 280%;">documentation</div>
	<div style="font-size: 80%;">v1.6.2 | 08.08.2019</div>
</div>

- [User interface](/manual/): Guide to the user interface.
- [Concepts](/concepts/): In-depth explanation of select topics important for *ella* end-users.
- [Technical documentation](/technical/): System setup and technical specification.
- [Release notes](/releasenotes/): Versions and changes.
- [Support](#support-and-requests): Send us an e-mail!
- [Official website](http://allel.es)

## About *ella* and availability

*ella* is a variant interpretation tool intended for clinical use, developed in close collaboration with users at the Department of Medical Genetics, Oslo University Hospital. 

The name *ella* is a word play on “alleles”:

<div style="text-indent: 4%;">
	<img src="./manual/img/logo_explanation.png">
</div>

*ella* is open source and available under a MIT licence, see the [official website](http://allel.es).

## Support and requests

For support and suggestions, [send us an e-mail](ma&#105;lt&#111;&#58;&#101;%6&#67;la&#37;2&#68;s&#117;pport&#64;m&#101;&#100;i&#115;&#105;&#110;&#46;%75i%&#54;F&#46;n%&#54;F)

::: warning NOTE
If you notice that functionality has stopped working or other unusual behaviour in *ella*, first try a **force refresh** of the browser cache: Press `Shift + F5`. This will ensure you get the latest setup, especially after updates.
:::
