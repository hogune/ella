# Navigating ella

The user interface in *ella* is organised in different pages, which are reached by choosing an analysis/interpretation or through buttons/links in the top bar. 

For in-depth descriptions, go to the referenced pages:

<hr>

**OVERVIEW page**  
*Data and worklists*
  
<div style="text-indent: 4%;"><img src="./img/overview_btn.png"></div>

- [Choosing a sample or variant](/manual/choosing-sample-variant.md): Select from worklist or perform a search.
- [Data import](/manual/data-import.md): Import additional data from VCF/text.
- [Reanalyses](/manual/reanalyses.md): Perform reanalyses with other gene panel on existing data.
- [User information and warnings](/manual/user-info-warnings.md): Information and warnings related to user, variant or workflow.
- [Data export for Sanger](/manual/export-sanger.md): Export samples/variants to be verified by Sanger.

<hr>	  
	  
**INFO page**  
*Sample information and warnings*

<div style="text-indent: 4%;"><img src="./img/nav_info_btn.png"></div>

- [Info page](/manual/info-page.html)

<hr>

**CLASSIFCATION page**  
*Evaluate evidence and set a classification*

<div style="text-indent: 4%;"><img src="./img/nav_classification_btn.png"></div>

- [Classification page](/manual/classification-page.html): Overview and basics.
- [Top bar](/manual/top-bar.html): Info and actions.
    - [Work log](/manual/worklog.html): System and user messages related to current analysis/interpretation.
- [Side bar](/manual/side-bar.html): Variant list with tags.
    - [Filtered variants](/manual/filtered-variants.html): Review and add back filtered variants.
- Full mode (default): 
    - [Sections for evidence](/manual/evidence-sections.html): Evaluate annotation and studies.
    - [Section for classification](/manual/classification-section.html): Summarise and set ACMG criteria and classification.
- [Quick mode](/manual/quick-classification.md): Quickly classifiy or mark variants in a list.
- [Visual mode](/manual/visual.html): Visualize variants, bam files and tracks.
  	  
<hr>	

**REPORT page**  
*Generate a clinical report*

<div style="text-indent: 4%;"><img src="./img/nav_report_btn.png"></div>

- [Report page](/manual/report-page.html) 