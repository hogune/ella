---
title: Export for Sanger
---

# Export report for Sanger verification 

::: warning NOTE
Availability of this feature depends on the configuration for your user group.
:::

To quickly set up Sanger-verification of important variants, before the samples have been analysed, use the `VARIANT REPORT` button in the top right corner:

<div style="text-indent: 4%;"><img src="./img/variant_report_btn.png"></div>

This generates an Excel-sheet that contains all analysis IDs and variants for samples that have not yet been started, along with import date, gene panel and any previously reported classification. The column sanger\_verify (TRUE/FALSE) uses the same criteria for Sanger verification as in *ella*.
