import showImportModal from '../actions/showImportModal'

export default [
    showImportModal,
    {
        result: [],
        dismissed: []
    }
]
