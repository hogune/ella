export default function getWorkLogState() {
    return {
        message: null,
        messageIds: null,
        messageCount: 0,
        messages: null,
        showMessagesOnly: false
    }
}
